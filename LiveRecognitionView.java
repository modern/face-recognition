package liverecognition;

import org.jdesktop.application.Action; 
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.*; 
import java.awt.image.BufferedImage; 
import javax.swing.*; 
import java.util.List; 
import java.util.ArrayList;  
import Luxand.*; 
import Luxand.FSDK.*;
import Luxand.FSDKCam.*;
import java.io.IOException; 
import java.io.Serializable;
import java.util.Iterator; 
import java.util.logging.Level;
import java.util.logging.Logger;

public class LiveRecognitionView extends FrameView implements Serializable {
    
    private BufferedImage bufImage = null; 
    private FSDK_Features.ByReference facialFeatures; 
    private java.util.List<FSDK_Features.ByReference> facialFeaturesArray; 
    private int smoothingLength = 10; 
    public Icon sel = new ImageIcon("sel.png"); 
    public Icon nsel = new ImageIcon("nsel.png");
    public Icon bsel = new ImageIcon("bsel.png");
    public Color customBlue = new Color(16,29,52); 
    private Server iServer; 
    private Thread serverThread; 
    private int numberTem = 0;
    StringBuilder sbTem = new StringBuilder();
   
    public LiveRecognitionView(SingleFrameApplication app) { 
        super(app); 

        initComponents(); 
        
        final JPanel mainFrame = this.mainPanel; 
        
        mainFrame.setBackground(customBlue); 
        
        try { 
            int r = FSDK.ActivateLibrary("Y+RF/5ose7K4CPZwv7aMnX37mDe/AVanO422Ao12zSi24iKpZik8U9vYDRxYnJqxoNbxUw85p1KGW8D6mZ8CTIdTPifiw6FZxvRlldyoqYWNKDH6sJyZZxfC/iAn+QzzZqSjhfcoc1j5SAO7gk1T0z7J+DTAFUb+mipdhwtItuo=");
            if (r != FSDK.FSDKE_OK){
                JOptionPane.showMessageDialog(mainPanel, "Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)", "Error activating FaceSDK", JOptionPane.ERROR_MESSAGE); 
                System.exit(r);
            }
        } 
        catch(java.lang.UnsatisfiedLinkError e) {
            JOptionPane.showMessageDialog(mainPanel, e.toString(), "Link Error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }    
            
        FSDK.Initialize(); 
        
        FSDK.SetFaceDetectionParameters(false, false, 100);

        FSDK.SetFaceDetectionThreshold(3);

        
        FSDKCam.InitializeCapturing();
                
        TCameras cameraList = new TCameras(); 
        int count[] = new int[1];
        FSDKCam.GetCameraList(cameraList, count);
        if (count[0] == 0){ 
            JOptionPane.showMessageDialog(mainFrame, "Подключите камеру"); 
            System.exit(1);
        }
        
        String cameraName = cameraList.cameras[0];  
        
        FSDK_VideoFormats formatList = new FSDK_VideoFormats(); 
        FSDKCam.GetVideoFormatList(cameraName, formatList, count); 
        FSDKCam.SetVideoFormat(cameraName, formatList.formats[0]); 
        
        cameraHandle = new HCamera(); 
        int r = FSDKCam.OpenVideoCamera(cameraName, cameraHandle); 
        if (r != FSDK.FSDKE_OK){
            JOptionPane.showMessageDialog(mainFrame, "Ошибка открытия камеры"); 
            System.exit(r);
        }
        
        drawingTimerElem = new Timer(40, new ActionListener() { 
            public void actionPerformed(ActionEvent e) { 
                HImage imageHandle = new HImage(); 
                if (FSDKCam.GrabFrame(cameraHandle, imageHandle) == FSDK.FSDKE_OK){
                    Image awtImage[] = new Image[1]; 
                    if (FSDK.SaveImageToAWTImage(imageHandle, awtImage, FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_24BIT) == FSDK.FSDKE_OK){
                        
                        TFacePosition.ByReference facePosition = new TFacePosition.ByReference(); 
                        
                        if (FSDK.DetectFace(imageHandle, facePosition) == FSDK.FSDKE_OK){

                            facialFeatures = new FSDK_Features.ByReference(); 
                            FSDK.DetectFacialFeatures(imageHandle, facialFeatures);
                            
                            bufImage = new BufferedImage(awtImage[0].getWidth(null), awtImage[0].getHeight(null), BufferedImage.TYPE_INT_ARGB); 
                            Graphics gr = bufImage.getGraphics(); 
                            gr.drawImage(awtImage[0], 0, 0, null);
                            
                            gr.setColor(Color.green);
                            gr.drawRect(facePosition.xc - facePosition.w/2, facePosition.yc - facePosition.w/2, facePosition.w, facePosition.w);

                            gr.setColor(Color.BLUE); 
                            gr.drawRect(facialFeatures.features[0].x - 25, facialFeatures.features[0].y - 25, 50, 50);
                            
                            gr.setColor(Color.GREEN); 
                            gr.drawRect(facialFeatures.features[1].x - 20, facialFeatures.features[1].y - 20, 40, 40);

                            gr.setColor(Color.YELLOW); 
                            gr.drawRect(facialFeatures.features[2].x - 23, facialFeatures.features[2].y - 23, 46, 46);
                            
                            gr.setColor(Color.RED); 
                            gr.drawRect(facialFeatures.features[64].x - 35, facialFeatures.features[64].y - 20, 70, 40);

                        }    

                        mainFrame.getRootPane().getGraphics().drawImage((bufImage != null) ? bufImage : awtImage[0], 0, 0, null);
                    }
                    FSDK.FreeImage(imageHandle); 
                }
            }
        });
        
        drawingTimerRec = new Timer(40, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                HImage imageHandle = new HImage();
                if (FSDKCam.GrabFrame(cameraHandle, imageHandle) == FSDK.FSDKE_OK){
                    Image awtImage[] = new Image[1];
                    if (FSDK.SaveImageToAWTImage(imageHandle, awtImage, FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_24BIT) == FSDK.FSDKE_OK){
                        
                        bufImage = null;
                        TFacePosition.ByReference facePosition = new TFacePosition.ByReference();
                        
                        if (FSDK.DetectFace(imageHandle, facePosition) == FSDK.FSDKE_OK){
                            
                                bufImage = new BufferedImage(awtImage[0].getWidth(null), awtImage[0].getHeight(null), BufferedImage.TYPE_INT_ARGB);
                                Graphics gr = bufImage.getGraphics();
                                gr.drawImage(awtImage[0], 0, 0, null);
                                gr.setColor(Color.green);
                                gr.drawRect(facePosition.xc - facePosition.w/2, facePosition.yc - facePosition.w/2, facePosition.w, facePosition.w);

                                FSDK_FaceTemplate.ByReference template = new FSDK_FaceTemplate.ByReference();

                                if (programState == programStateRemember || programState == programStateRecognize)
 
                                FSDK.GetFaceTemplateInRegion(imageHandle, facePosition, template);

                                switch (programState) {
                                case programStateNormal:
                                    break;
                                    case programStateRemember:
                                        faceTemplates.add(template);
                                        sbTem = sbTem.append(template);
                                        
                                        jLabel1.setText("Количество проходов: " + Integer.toString(faceTemplates.size()));
                                        if (faceTemplates.size() > 9) {
                                            userName = (String)JOptionPane.showInputDialog(mainFrame, "Ваше имя:", "Введите ваше имя", JOptionPane.QUESTION_MESSAGE, null, null, "Вадим");
                                            programState = programStateRecognize;

                                        }
                                        break;
                                    
                                case programStateRecognize:
                                    boolean match = false;

                                    Iterator it = faceTemplates.iterator(); 
                                    while (it.hasNext()){ 
                                        float similarity[] = new float[] {0.0f};
                                        FSDK_FaceTemplate.ByReference t1 = (FSDK_FaceTemplate.ByReference)it.next();
                                        FSDK.MatchFaces(t1, template, similarity); 
                                        float threshold[] = new float[] {0.0f};
                                        FSDK.GetMatchingThresholdAtFAR(0.01f, threshold); 
                                        if (similarity[0] > threshold[0]){
                                            match = true;
                                            break;
                                        }
                                    }
                                    if (match){ 
                                        gr.setFont(new Font("Arial", Font.BOLD, 16));
                                        FontMetrics fm = gr.getFontMetrics();
                                        java.awt.geom.Rectangle2D textRect = fm.getStringBounds(userName, gr);
                                        gr.drawString(userName, (int)(facePosition.xc - textRect.getWidth()/2), (int)(facePosition.yc + facePosition.w/2 + textRect.getHeight()));
                                    }
                                    break;
                                } 
                            
                        }
                        mainFrame.getRootPane().getGraphics().drawImage((bufImage != null) ? bufImage : awtImage[0], 0, 0, null);
                    }
                    FSDK.FreeImage(imageHandle); 
                }
            }
        });
        
        
        facialFeaturesArray = new java.util.LinkedList<FSDK_Features.ByReference>();

        drawingTimerPoints = new Timer(40, new ActionListener() {
             public void actionPerformed(ActionEvent e) {
                HImage imageHandle = new HImage();
                if (FSDKCam.GrabFrame(cameraHandle, imageHandle) == FSDK.FSDKE_OK){
                    Image awtImage[] = new Image[1];
                    if (FSDK.SaveImageToAWTImage(imageHandle, awtImage, FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_24BIT) == FSDK.FSDKE_OK){
                        BufferedImage bufImage = null;
                        TFacePosition.ByReference facePosition = new TFacePosition.ByReference();
                        
                        if (FSDK.FSDKE_OK == FSDK.DetectFace(imageHandle, facePosition)){
                            FSDK_Features.ByReference facialFeatures = new FSDK_Features.ByReference();
                            FSDK.DetectFacialFeaturesInRegion(imageHandle, facePosition, facialFeatures); 
                            
                            bufImage = new BufferedImage(awtImage[0].getWidth(null), awtImage[0].getHeight(null), BufferedImage.TYPE_INT_ARGB);
                            Graphics gr = bufImage.getGraphics(); 
                            gr.drawImage(awtImage[0], 0, 0, null);
                            
                            gr.setColor(Color.blue);
                            for (int i = 0; i < FSDK.FSDK_FACIAL_FEATURE_COUNT; i++){
                                gr.fillOval(facialFeatures.features[i].x, facialFeatures.features[i].y, 5, 5);
                            }
                            gr.setColor(Color.green);
                            int left = facePosition.xc - (2 * facePosition.w) / 3;
                            int top = facePosition.yc - facePosition.w / 2;
                            gr.drawRect(left, top, 4 * facePosition.w / 3, 4 * facePosition.w / 3);
                        }       
                        mainFrame.getRootPane().getGraphics().drawImage((bufImage != null) ? bufImage : awtImage[0], 0, 0, null);
                    }
                    FSDK.FreeImage(imageHandle);
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jCheckBox4 = new javax.swing.JCheckBox();
        buttonGroup1 = new javax.swing.ButtonGroup();

        mainPanel.setName("mainPanel"); 

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(liverecognition.LiveRecognitionApp.class).getContext().getActionMap(LiveRecognitionView.class, this);
        jButton2.setAction(actionMap.get("buttonRemember")); 
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(liverecognition.LiveRecognitionApp.class).getContext().getResourceMap(LiveRecognitionView.class);
        jButton2.setFont(resourceMap.getFont("jButton2.font"));
        jButton2.setForeground(resourceMap.getColor("jButton2.foreground")); 
        jButton2.setText(resourceMap.getString("jButton2.text")); 
        jButton2.setName("jButton2"); 

        buttonGroup1.add(jCheckBox2);
        jCheckBox2.setFont(resourceMap.getFont("jCheckBox2.font")); 
        jCheckBox2.setForeground(resourceMap.getColor("jCheckBox2.foreground")); 
        jCheckBox2.setText(resourceMap.getString("jCheckBox2.text")); 
        jCheckBox2.setName("jCheckBox2"); 
        jCheckBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox2ItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jCheckBox1);
        jCheckBox1.setFont(resourceMap.getFont("jCheckBox1.font")); 
        jCheckBox1.setForeground(resourceMap.getColor("jCheckBox1.foreground")); 
        jCheckBox1.setText(resourceMap.getString("jCheckBox1.text")); 
        jCheckBox1.setName("jCheckBox1"); 
        jCheckBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox1ItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jCheckBox3);
        jCheckBox3.setFont(resourceMap.getFont("jCheckBox3.font"));  
        jCheckBox3.setForeground(resourceMap.getColor("jCheckBox3.foreground"));  
        jCheckBox3.setSelected(true);
        jCheckBox3.setText(resourceMap.getString("jCheckBox3.text"));  
        jCheckBox3.setName("jCheckBox3");  
        jCheckBox3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox3ItemStateChanged(evt);
            }
        });

        jPanel1.setBackground(resourceMap.getColor("jPanel1.background"));  
        jPanel1.setName("jPanel1");  

        jLabel1.setFont(resourceMap.getFont("jLabel1.font"));  
        jLabel1.setForeground(resourceMap.getColor("jLabel1.foreground"));  
        jLabel1.setText(resourceMap.getString("jLabel1.text"));  
        jLabel1.setName("jLabel1");  

        jLabel2.setFont(resourceMap.getFont("jLabel2.font"));  
        jLabel2.setText(resourceMap.getString("jLabel2.text"));  
        jLabel2.setName("jLabel2");  

        jCheckBox4.setBackground(resourceMap.getColor("jCheckBox4.background"));  
        jCheckBox4.setText(resourceMap.getString("jCheckBox4.text"));  
        jCheckBox4.setName("jCheckBox4");  
        jCheckBox4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox4ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jCheckBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel1.getAccessibleContext().setAccessibleName(resourceMap.getString("jLabel1.AccessibleContext.accessibleName"));  

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox3)
                .addGap(0, 21, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap(393, Short.MAX_VALUE)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        setComponent(mainPanel);
    }

    private void jCheckBox1ItemStateChanged(java.awt.event.ItemEvent evt) {

        jCheckBox1.setIcon(sel);
        jCheckBox2.setIcon(nsel);
        jCheckBox3.setIcon(nsel);
        
        if(drawingTimerRec.isRunning()){ 
            drawingTimerRec.stop();
        } else if (drawingTimerPoints.isRunning()) {
            drawingTimerPoints.stop();
        } else if (drawingTimerElem.isRunning()) {
            drawingTimerElem.stop();
        }
        jButton2.setEnabled(true);
        drawingTimerRec.start();
    }

    private void jCheckBox2ItemStateChanged(java.awt.event.ItemEvent evt) {

        jCheckBox1.setIcon(nsel);
        jCheckBox2.setIcon(sel);
        jCheckBox3.setIcon(nsel);
        
        if(drawingTimerRec.isRunning()){ 
            drawingTimerRec.stop();
        } else if (drawingTimerPoints.isRunning()) {
            drawingTimerPoints.stop();
        } else if (drawingTimerElem.isRunning()) {
            drawingTimerElem.stop();
        }
        jButton2.setEnabled(false);
        drawingTimerPoints.start();
    }

    private void jCheckBox3ItemStateChanged(java.awt.event.ItemEvent evt) {
	
        jCheckBox1.setIcon(nsel);
        jCheckBox2.setIcon(nsel);
        jCheckBox3.setIcon(sel);
        
        if(drawingTimerRec.isRunning()){ 
            drawingTimerRec.stop();
        } else if (drawingTimerPoints.isRunning()) {
            drawingTimerPoints.stop();
        } else if (drawingTimerElem.isRunning()) {
            drawingTimerElem.stop();
        }
        jButton2.setEnabled(false);
        drawingTimerElem.start();
    }

    private void jCheckBox4ItemStateChanged(java.awt.event.ItemEvent evt) {
	
        if(jCheckBox4.isSelected()) {
            iServer = new Server();
            serverThread = new Thread(iServer);
            serverThread.start(); 
        } else {
            try {
                serverThread.interrupt();
                
                if(iServer.flag == 0) {
                    iServer.server.close();
                } else {
                    iServer.out.close();
                    iServer.in.close();
                    iServer.client.close(); 
                    iServer.server.close();
                }                
            } catch (IOException ex) {
                Logger.getLogger(LiveRecognitionView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Action
    public void buttonRemember() { 
        faceTemplates = new ArrayList<FSDK_FaceTemplate.ByReference>();
        faceTemplates.clear();
        
        faceTemp = new ArrayList<FSDK_FaceTemplate.ByReference>();
        faceTemp.clear();

        programState = programStateRemember;
        this.jLabel1.setText("Смотрите в камеру");
        
    }

    public void closeCamera(){ 
        FSDKCam.CloseVideoCamera(cameraHandle);
        FSDKCam.FinalizeCapturing();
        FSDK.Finalize();
    }
   
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton2;
    public javax.swing.JCheckBox jCheckBox1;
    public javax.swing.JCheckBox jCheckBox2;
    public javax.swing.JCheckBox jCheckBox3;
    public javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel mainPanel;
    
    public  Timer drawingTimerElem; 
    public  Timer drawingTimerRec; 
    public  Timer drawingTimerPoints; 
    private HCamera cameraHandle;
    private String userName;
    private List<FSDK_FaceTemplate.ByReference> faceTemplates; 
    
    private List<FSDK_FaceTemplate.ByReference> faceTemp;
    
    final int programStateNormal = 0;
    final int programStateRemember = 1;
    final int programStateRecognize = 2;
    private int programState = programStateNormal;
    
}
