package liverecognition; 

import org.jdesktop.application.Application; 
import org.jdesktop.application.SingleFrameApplication;
import java.awt.event.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level; 
import java.util.logging.Logger;

public class LiveRecognitionApp extends SingleFrameApplication {
    public static LiveRecognitionView liveRecognitionViewFrame;

    @Override protected void startup() {

        liveRecognitionViewFrame = new LiveRecognitionView(this); 
        
        try {
            liveRecognitionViewFrame.jLabel2.setText(""+ InetAddress.getLocalHost()); 
        } catch (UnknownHostException ex) { 
            Logger.getLogger(LiveRecognitionApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        liveRecognitionViewFrame.jCheckBox1.setIcon(liveRecognitionViewFrame.nsel);
        liveRecognitionViewFrame.jCheckBox2.setIcon(liveRecognitionViewFrame.nsel);
        liveRecognitionViewFrame.jCheckBox3.setIcon(liveRecognitionViewFrame.sel);
        liveRecognitionViewFrame.jCheckBox1.setBackground(liveRecognitionViewFrame.customBlue);
        liveRecognitionViewFrame.jCheckBox2.setBackground(liveRecognitionViewFrame.customBlue);
        liveRecognitionViewFrame.jCheckBox3.setBackground(liveRecognitionViewFrame.customBlue);
        
        liveRecognitionViewFrame.jButton2.setIcon(liveRecognitionViewFrame.bsel);
        liveRecognitionViewFrame.jButton2.setBackground(liveRecognitionViewFrame.customBlue);
        liveRecognitionViewFrame.jButton2.setBorderPainted(false);  
        liveRecognitionViewFrame.jButton2.setFocusPainted(false);  
        liveRecognitionViewFrame.jButton2.setContentAreaFilled(false);
        
        show(liveRecognitionViewFrame); 

        liveRecognitionViewFrame.drawingTimerElem.start();  
        liveRecognitionViewFrame.jButton2.setEnabled(false); 
        
    }

    @Override protected void configureWindow(java.awt.Window root) {
        root.addWindowListener(new WindowAdapter() { 
            @Override
            public void windowClosing(WindowEvent e) { 
                liveRecognitionViewFrame.drawingTimerElem.stop();
                liveRecognitionViewFrame.drawingTimerPoints.stop();
                liveRecognitionViewFrame.drawingTimerRec.stop();
                try{
                    Thread.sleep(40);
                }
                catch (java.lang.InterruptedException exception){
                }
                liveRecognitionViewFrame.closeCamera();
            }
        });
    }

    public static LiveRecognitionApp getApplication() {
        return Application.getInstance(LiveRecognitionApp.class);
    }

    public static void main(String[] args) {
        launch(LiveRecognitionApp.class, args);
    }
}
